// ColorListBox.cpp : implementation file

//-------------------------------------------------------------------
//
//	CColorListBox class - 
//		A CListBox-derived class with optional colored items.
//
//		Version: 1.0	01/10/1998 Copyright � Patrice Godard
//
//		Version: 2.0	09/17/1999 Copyright � Paul M. Meidinger
//
//-------------------------------------------------------------------

#include "pch.h"
#include "ColorListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorListBox

//-------------------------------------------------------------------
//
CColorListBox::CColorListBox()
//
// Return Value:	None.
//
// Parameters	:	None.
//
// Remarks		:	Standard constructor.
//
{
}	// CColorListBox

//-------------------------------------------------------------------
//
CColorListBox::~CColorListBox()
//
// Return Value:	None.
//
// Parameters	:	None.
//
// Remarks		:	Destructor.
//
{
}	// ~CColorListBox()


BEGIN_MESSAGE_MAP(CColorListBox, CListBox)
	//{{AFX_MSG_MAP(CColorListBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorListBox message handlers

//-------------------------------------------------------------------
//
void CColorListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS) 
//
// Return Value:	None.
//
// Parameters	:	lpDIS - A long pointer to a DRAWITEMSTRUCT structure 
//							that contains information about the type of drawing required.
//
// Remarks		:	Called by the framework when a visual aspect of 
//						an owner-draw list box changes. 
//
{
	if (static_cast<int>(lpDIS->itemID) < 0)
		return; 

	CDC      dc;
	CRect      rcItem(lpDIS->rcItem);
	UINT      nIndex = lpDIS->itemID;
	COLORREF   rgbBkgnd = ::GetSysColor((lpDIS->itemState & ODS_SELECTED) ? COLOR_HIGHLIGHT : COLOR_WINDOW);
	dc.Attach(lpDIS->hDC);
	CBrush br(rgbBkgnd);
	dc.FillRect(rcItem, &br);
	if (lpDIS->itemState & ODS_FOCUS)
		dc.DrawFocusRect(rcItem);
	if (nIndex != static_cast<UINT>(-1)) {
		// The text color is stored as the item data.
		COLORREF rgbText = (lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHTTEXT) : static_cast<COLORREF>(GetItemData(nIndex));
		
		int nBkMode = dc.SetBkMode(TRANSPARENT);
		
		CString str;
		GetText(nIndex, str);

		UINT nFormat = DT_LEFT | DT_SINGLELINE | DT_VCENTER;
		if (GetStyle() & LBS_USETABSTOPS)
		nFormat |= DT_EXPANDTABS;

		rgbText = dc.SetTextColor(rgbText);

		// Calculate the rectangle size before drawing the text.
		dc.DrawText(str, -1, &rcItem, nFormat | DT_CALCRECT);
		dc.DrawText(str, -1, &rcItem, nFormat);

		dc.SetTextColor(rgbText);
		dc.SetBkMode(nBkMode);
	}
	dc.Detach();
	return;
}	// DrawItem

//-------------------------------------------------------------------
//
void CColorListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
//
// Return Value:	None.
//
// Parameters	:	lpMIS - A long pointer to a 
//							MEASUREITEMSTRUCT structure.
//
// Remarks		:	Called by the framework when a list box with 
//						an owner-draw style is created. 
//
{
	return;// ### Is the default list box item height the same as
	// the menu check height???
	//lpMIS->itemHeight = ::GetSystemMetrics(SM_CYMENUCHECK);
}	// MeasureItem

//-------------------------------------------------------------------
//
int CColorListBox::AddString(LPCTSTR lpszItem)
//
// Return Value:	The zero-based index to the string in the list box. 
//						The return value is LB_ERR if an error occurs; the 
//						return value is LB_ERRSPACE if insufficient space 
//						is available to store the new string.
//
// Parameters	:	lpszItem - Points to the nullptr-terminated 
//							string that is to be added.
//
// Remarks		:	Call this member function to add a string to a list 
//						box. Provided because CListBox::AddString is NOT
//						a virtual function.
//
{
	return static_cast<CListBox*>(this)->AddString(lpszItem);
}	// AddString

//-------------------------------------------------------------------
//
int CColorListBox::AddString(LPCTSTR lpszItem, COLORREF rgb)
//
// Return Value:	The zero-based index to the string in the list box. 
//						The return value is LB_ERR if an error occurs; the 
//						return value is LB_ERRSPACE if insufficient space 
//						is available to store the new string.
//
// Parameters	:	lpszItem - Points to the nullptr-terminated 
//							string that is to be added.
//						rgb - Specifies the color to be associated with the item.
//
// Remarks		:	Call this member function to add a string to a list 
//						box with a custom color.
//
{
	int nItem = AddString(lpszItem);
	if (nItem >= 0)
		SetItemData(nItem, rgb);
	return nItem;
}	// AddString

//-------------------------------------------------------------------
//
int CColorListBox::InsertString(int nIndex, LPCTSTR lpszItem)
//
// Return Value:	The zero-based index of the position at which the 
//						string was inserted. The return value is LB_ERR if 
//						an error occurs; the return value is LB_ERRSPACE if 
//						insufficient space is available to store the new string.
//
// Parameters	:	nIndex - Specifies the zero-based index of the position
//							to insert the string. If this parameter is �1, the string
//							is added to the end of the list.
//						lpszItem - Points to the nullptr-terminated string that 
//							is to be inserted.
//
// Remarks		:	Inserts a string into the list box.	Provided because 
//						CListBox::InsertString is NOT a virtual function.
//
{
	return static_cast<CListBox*>(this)->InsertString(nIndex, lpszItem);
}	// InsertString

//-------------------------------------------------------------------
//
int CColorListBox::InsertString(int nIndex, LPCTSTR lpszItem, COLORREF rgb)
//
// Return Value:	The zero-based index of the position at which the 
//						string was inserted. The return value is LB_ERR if 
//						an error occurs; the return value is LB_ERRSPACE if 
//						insufficient space is available to store the new string.
//
// Parameters	:	nIndex - Specifies the zero-based index of the position
//							to insert the string. If this parameter is �1, the string
//							is added to the end of the list.
//						lpszItem - Points to the nullptr-terminated string that 
//							is to be inserted.
//						rgb - Specifies the color to be associated with the item.
//
// Remarks		:	Inserts a colored string into the list box.
//
{
	int nItem = static_cast<CListBox*>(this)->InsertString(nIndex,lpszItem);
	if (nItem >= 0)
		SetItemData(nItem, rgb);
	return nItem;
}	// InsertString

//-------------------------------------------------------------------
//
void CColorListBox::SetItemColor(int nIndex, COLORREF rgb)
//
// Return Value:	None.
//
// Parameters	:	nIndex - Specifies the zero-based index of the item.
//						rgb - Specifies the color to be associated with the item.
//
// Remarks		:	Sets the 32-bit value associated with the specified
//						item in the list box.
//
{
	SetItemData(nIndex, rgb);	
	RedrawWindow();
}	// SetItemColor
