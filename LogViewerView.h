
// LogViewerView.h : interface of the CLogViewerView class
//

#pragma once

#include "CMFCListView.h"

class CLogViewerView : public CListView
{
protected: // create from serialization only
	CLogViewerView() noexcept;
	DECLARE_DYNCREATE(CLogViewerView)

// Attributes
public:
	CLogViewerDoc* GetDocument() const;

// Operations
public:
// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct

	virtual void GetDispInfo(LVITEM* pItem);
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	void UpdateFilter(CString strCurQuery);
	void OnCustomDrawList(NMHDR* pNMHDR, LRESULT* pResult);
// Implementation
public:
	virtual ~CLogViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()

	bool useCache;
	std::map<int, std::vector<std::string>> cache;
	CFindReplaceDialog* m_pFindDialog;
public:
	int FindItem(int iStart, LPNMLVFINDITEM plvfi);
	void PrepareCache(int iFrom, int iTo);
	afx_msg void OnEditFind();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

};

#ifndef _DEBUG  // debug version in LogViewerView.cpp
inline CLogViewerDoc* CLogViewerView::GetDocument() const
   { return reinterpret_cast<CLogViewerDoc*>(m_pDocument); }
#endif

