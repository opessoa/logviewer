
#pragma once

#include "IMessage.h"
#include "ColorListBox.h"
#include <string>
#include <map>
#include <memory>

/////////////////////////////////////////////////////////////////////////////
// COutputList window

class COutputList : public CColorListBox
{
// Construction
public:
	COutputList() noexcept;

// Implementation
public:
	virtual ~COutputList();
protected:
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEditCopy();
	afx_msg void OnEditClear();
	afx_msg void OnViewOutput();

	DECLARE_MESSAGE_MAP()

};

class COutputWnd : public CDockablePane, public IMessage
{
// Construction
public:
	COutputWnd() noexcept;

	void UpdateFonts();
	CFont m_font;


// Attributes
protected:
	CMFCTabCtrl	m_wndTabs;

	std::map<std::string, std::shared_ptr<COutputList>> outputListMap;
protected:
	void AdjustHorzScroll(CListBox& wndListBox);

// Implementation
public:
	virtual ~COutputWnd();
	// Inherited via IMessage
	virtual void AddMessage(char const* const _Format, ...) override;
	void AddMessage(std::string strMsg);
	std::shared_ptr<COutputList> CreateOutputList(std::string sName);
	void AddTextToWindow(std::string window, CString sText, COLORREF color);
	void AddText(std::string sWindow, std::string strMsg, COLORREF color = { 0 });

protected:
	afx_msg LRESULT OnCreateOutputList(WPARAM wParam, LPARAM lParam);
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};

