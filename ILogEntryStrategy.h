#pragma once
#include <string>
#include <vector>

using namespace std;

class ILogEntryStrategy
{
protected:
    bool filtered;
public:
    virtual bool loadFile(std::string fileName) = 0; 
    virtual const std::vector<std::string>& operator[](int lineNum) = 0; 
    virtual size_t Filter(std::string filterText) = 0;
    virtual size_t GetRecordCount() = 0;
  
    bool Filtered() {
        return filtered;
    }
};