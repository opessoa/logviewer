#pragma once
#include "IMessage.h"

class IApp
{
public:

	virtual IMessage* GetIMessage() = 0;
};