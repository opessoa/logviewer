#pragma once
#include "ILogEntryStrategy.h"
#include "SQLite/sqlite3.h"
#include "ToSql.h"
#include <memory>

class SQLiteLogEntryStrategy : public ILogEntryStrategy
{
public:
	SQLiteLogEntryStrategy();
	virtual	~SQLiteLogEntryStrategy();
	sqlite3* OpenDB();
public:
	bool InsertItem(const std::vector<std::string>& line, sqlite3_stmt* stmt);
	// Inherited via ILogEntryStrategy
	virtual bool loadFile(std::string fileName) override;
	virtual const std::vector<std::string>& operator[](int lineNum) override;
	virtual size_t Filter(std::string filterText) override;
	virtual size_t GetRecordCount() override;

protected:
	std::vector<std::string> fromString(int lineNum, const std::string& lineText);
	static int callback(void* NotUsed, int argc, char** argv, char** azColName);
	bool CreateTable();
	size_t count(std::string& sql);
protected:
	SQLitePtr dbFilter;
	SQLitePtr dbNoFilter;

	sqlite3_stmt* filterStatement;
	sqlite3_stmt* noFilterStatement;

	size_t filteredCount;
	size_t noFilteredCount;

	std::string documentFileName;
};

