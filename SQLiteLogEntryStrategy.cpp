#include "pch.h"
#include "SQLiteLogEntryStrategy.h"
#include "ToSql.h"

SQLiteLogEntryStrategy::SQLiteLogEntryStrategy():documentFileName(std::to_string(time(NULL)))
{
	filtered = false;
	filterStatement = nullptr;
	noFilterStatement = nullptr;

	
	dbNoFilter.SetDB(OpenDB());
	dbFilter.SetDB(OpenDB());
	//SQLiteHelper::loadOrSaveDb(dbNoFilter.get(), "file:memdb1?mode=memory&cache=shared", 1);

}

SQLiteLogEntryStrategy::~SQLiteLogEntryStrategy()
{
	if (noFilterStatement) {
		sqlite3_finalize(noFilterStatement);
	}
	if (filterStatement) {
		sqlite3_finalize(filterStatement);
	}
}

sqlite3* SQLiteLogEntryStrategy::OpenDB() {
	sqlite3* db;
	int rc;
	//rc = sqlite3_open("test.db", &db);
	//rc = sqlite3_open(":memory:", &db);
	//rc = sqlite3_open("file:memory?cache=shared", &db);
	std::string oMem = "file:"+ documentFileName +"?mode=memory&cache=shared";
	rc = sqlite3_open(oMem.c_str(), &db);
	if (rc) {
		std::cout << "Can't open database: %s\n" << sqlite3_errmsg(db);
		return false;
	}
	return db;
}

int SQLiteLogEntryStrategy::callback(void* NotUsed, int argc, char** argv, char** azColName) {
	int i;
	for (i = 0; i < argc; i++) {
		cout << azColName[i] << " : " << (argv[i] ? argv[i] : "NULL") << endl;
	}
	cout << endl;
	return 0;
}

bool SQLiteLogEntryStrategy::CreateTable() {
	/* Create SQL statement */
	char* zErrMsg = 0;
	std::string sql = "CREATE TABLE if not exists LOG("  \
		"ID INT," \
		"NAME           TEXT," \
		"timestamp      TEXT," \
		"N1      INT," \
		"LIB     TEXT," \
		"N2     INT," \
		"FILE    TEXT," \
		"LINE   INT," \
		"MSG            TEXT);";

	/* Execute SQL statement */
	int rc = sqlite3_exec(dbNoFilter.get(), sql.c_str(), nullptr, 0, &zErrMsg);

	if (rc != SQLITE_OK) {
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

size_t SQLiteLogEntryStrategy::count(std::string& sql)
{
	size_t counter = 0;
	std::string s;
	s = sql.substr(0, 7);
	s+= " count(*) ";
	s+= sql.substr(9);

	sqlite3_stmt* stmt;
	if (filtered) {
		if (sqlite3_prepare(dbFilter.get(), s.c_str(), -1, &stmt, NULL) == SQLITE_OK) {
			if (SQLITE_ROW == sqlite3_step(stmt)) {
				counter = sqlite3_column_int(stmt, 0);
			}
		}
		else {
			CString s( sqlite3_errmsg(dbFilter.get()));
			AfxMessageBox(s);
		}
		sqlite3_finalize(stmt);
	}
	else
	{
		if (sqlite3_prepare(dbNoFilter.get(), s.c_str(), -1, &stmt, NULL) == SQLITE_OK) {
			if (SQLITE_ROW == sqlite3_step(stmt)) {
				counter = sqlite3_column_int(stmt, 0);
			}
		}
		else {
			CString s(sqlite3_errmsg(dbNoFilter.get()));
			AfxMessageBox(s);
		}
		sqlite3_finalize(stmt);
	}
	return counter;
}


bool SQLiteLogEntryStrategy::InsertItem(const std::vector<std::string>& line,sqlite3_stmt*stmt) {
	if (line.size() == 0)
		return false;
	
	//Real computation loop is a bit more complicated
	for (size_t i = 0; i < line.size(); ++i) {
		sqlite3_bind_text(stmt, i+1, line[i].c_str(),-1,nullptr);
	}
    sqlite3_step(stmt);
    sqlite3_reset(stmt);
	return true;
}

bool SQLiteLogEntryStrategy::loadFile(std::string fileName)
{
	documentFileName = fileName;

	ifstream file;

	file.open(fileName, std::ios_base::in | std::ios_base::binary);

	const std::string type[] = { "WARNING", "ERROR", "INFO", "FATAL", "DEBUG" };

	std::string totalbuffer;
	std::string buffer;
	int line = 0;

	if (!CreateTable()) {
		sqlite3_close(dbNoFilter.get());
		return false;
	}
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(dbNoFilter.get(), "INSERT INTO log (ID,NAME,timestamp,N1,lib,N2,FILE,LINE,MSG) VALUES (?, ?, ?, ?,?, ?, ?, ?,?);", -1, &stmt, nullptr);

	sqlite3_exec(dbNoFilter.get(), "BEGIN;", nullptr, nullptr, nullptr);
	int curLine=0;
	while (std::getline(file, buffer, '\n')) {
		line++;
		if ((buffer[0] == 'W' && buffer[1] == 'A') ||
			(buffer[0] == 'E' && buffer[1] == 'R') ||
			(buffer[0] == 'I' && buffer[1] == 'N') ||
			(buffer[0] == 'F' && buffer[1] == 'A') ||
			(buffer[0] == 'D' && buffer[1] == 'E')
			)
		{
			if (totalbuffer.size() && totalbuffer != buffer)
			{
				InsertItem(fromString(curLine, totalbuffer), stmt);
			}
			curLine = line;
			totalbuffer = buffer;
		}
		else {
			totalbuffer += buffer;
		}
	}
	file.close();
	if (totalbuffer.size() && totalbuffer != buffer)
	{
		InsertItem(fromString(curLine, totalbuffer),stmt);
	}
	sqlite3_exec(dbNoFilter.get(), "COMMIT;", nullptr, nullptr, nullptr);

	SQLiteHelper::loadOrSaveDb(dbNoFilter.get(), (documentFileName+".sqlite").c_str(), 1);

	sqlite3_finalize(stmt);
	string sql("select * from log;");
	noFilteredCount = count(sql);
	if (sqlite3_prepare(dbNoFilter.get(), sql.c_str(), -1, &noFilterStatement, NULL) != SQLITE_OK) {
		CString s(sqlite3_errmsg(dbNoFilter.get()));
	}

    return true;
}

std::vector<std::string> SQLiteLogEntryStrategy::fromString(int lineNum, const std::string& lineText) {
	std::vector<std::string> line;
	size_t befPos, pos;
	if (lineText[0] == '\n')
		befPos = 1;
	pos = lineText.find('\t', 0);
	if (pos) {
		line.push_back(std::to_string(lineNum));
		line.push_back(lineText.substr(0, pos));
		befPos = pos + 1;
		pos = lineText.find('\t', befPos);
		if (pos) {
			line.push_back(lineText.substr(befPos, pos - befPos));
			befPos = pos + 1;
			pos = lineText.find('\t', befPos);
			if (pos) {
				line.push_back(lineText.substr(befPos, pos - befPos));
				befPos = pos + 1;
				pos = lineText.find('\t', befPos);
				if (pos) {
					line.push_back(lineText.substr(befPos, pos - befPos));
					befPos = pos + 1;
					pos = lineText.find('\t', befPos);
					if (pos) {
						line.push_back(lineText.substr(befPos, pos - befPos));
						befPos = pos + 1;
						pos = lineText.find('@', befPos);
						if (pos) {
							line.push_back(lineText.substr(befPos, pos - befPos));
							befPos = pos + 1;
							pos = lineText.find('\t', befPos);
							if (pos) {
								line.push_back(lineText.substr(befPos, pos - befPos));
								befPos = pos + 1;
								line.push_back(lineText.substr(befPos, -1));
							}
						}
					}

				}
			}
		}

	}
	return line;
}


const std::vector<std::string>& SQLiteLogEntryStrategy::operator[](int lineNum)
{
	static int lastLine = 0;
	static std::vector<std::string> line;
	if (lineNum != 0 && lastLine == lineNum)
	{
		return line;
	}
	line.clear();
	
	sqlite3_stmt* pCurrentStmt=nullptr;
	if (filtered) {
		pCurrentStmt = filterStatement;
	}
	else {
		pCurrentStmt = noFilterStatement;
	}

	int sqlRet = SQLITE_ROW;

	if (lineNum == (lastLine + 1)) {
		sqlRet = sqlite3_step(pCurrentStmt);
	}
	else
	{
		sqlite3_reset(pCurrentStmt);
		sqlRet = sqlite3_step(pCurrentStmt);
		for (int n = 1; n < lineNum && sqlRet == SQLITE_ROW; n++) {
			sqlRet = sqlite3_step(pCurrentStmt);
		}
	}
	lastLine = lineNum;

	for (int n = 0; n < 9; n++) {
		line.push_back(std::string( (const char*)sqlite3_column_text(pCurrentStmt, n) ));
	}

	return line;
}

size_t SQLiteLogEntryStrategy::Filter(std::string filterText)
{
	if (filterStatement) {
		sqlite3_finalize(filterStatement);
	}

	if (filterText.empty()){
		filtered = false;
		return noFilteredCount;
	}

	filtered = true;
	string sql("select * from log where msg like '%"); 
	sql += filterText;
	sql += "%';";

	filteredCount = count(sql);

	sqlite3_prepare(dbFilter.get(), sql.c_str(), static_cast<int>(sql.size()), &filterStatement, NULL);

    return size_t();
}

size_t SQLiteLogEntryStrategy::GetRecordCount()
{
	return filtered ? filteredCount : noFilteredCount;
}
