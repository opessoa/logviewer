#pragma once
#include "ILogEntryStrategy.h"

class InMemoryLogEntryStrategy : public ILogEntryStrategy
{
public:
	InMemoryLogEntryStrategy();

public:
	// Inherited via ILogEntryStrategy
	virtual bool loadFile(std::string fileName) override;
	virtual const std::vector<std::string>& operator[](int lineNum) override;
	virtual size_t Filter(std::string filterText) override;
	virtual size_t GetRecordCount() override;


	const std::vector<std::vector<std::string>>& GetLinePosition() {
		if (filtered) {
			return lineVectorFiltered;
		}
		return lineVector;
	}
protected:
	
	std::vector<std::string> fromString(int lineNum, const std::string& lineText);

	std::vector<std::vector<std::string>> lineVector;
	std::vector<std::vector<std::string>> lineVectorFiltered;
};

