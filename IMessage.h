#pragma once
class IMessage
{
public:

	virtual void AddMessage(char const* const _Format, ...) = 0;
};