#pragma once
#include <afxwin.h>

class CMFCListView : public CCtrlView
{
	DECLARE_DYNCREATE(CMFCListView)

	// Construction
public:
	CMFCListView();

	// Attributes
public:
	CMFCListCtrl& GetListCtrl() const;

	// Overridables
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

protected:
	void RemoveImageList(int nImageList);
	virtual BOOL OnChildNotify(UINT, WPARAM, LPARAM, LRESULT*);

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg void OnNcDestroy();

	DECLARE_MESSAGE_MAP()
};

