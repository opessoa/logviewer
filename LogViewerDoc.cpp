
// LogViewerDoc.cpp : implementation of the CLogViewerDoc class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "LogViewer.h"
#endif

#include "LogViewerDoc.h"

#include <propkey.h>
#include <fstream>
#include <chrono>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#include <future>
#include <regex>
#include <algorithm>
#include <execution>
#include "IApp.h"
#include "InFileLogEntryStrategy.h"
#include "InMemoryLogEntryStrategy.h"
#include "SQLiteLogEntryStrategy.h"
// CLogViewerDoc

IMPLEMENT_DYNCREATE(CLogViewerDoc, CDocument)

BEGIN_MESSAGE_MAP(CLogViewerDoc, CDocument)
END_MESSAGE_MAP()


// CLogViewerDoc construction/destruction

CLogViewerDoc::CLogViewerDoc() noexcept : strategy(new SQLiteLogEntryStrategy())
{
}

CLogViewerDoc::~CLogViewerDoc()
{
}


std::vector<std::string> CLogViewerDoc::operator[](int lineNum)
{
	return (*strategy)[lineNum];
}

void CLogViewerDoc::InitDocument(LPCTSTR lpszPathName)
{
	auto start = std::chrono::steady_clock::now();

	std::wstring ws(lpszPathName);
	strategy->loadFile(std::string(ws.begin(), ws.end()));
	
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;

	getIMessage()->AddMessage("InitDocument -> %0.2f", elapsed_seconds);
}

BOOL CLogViewerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

size_t CLogViewerDoc::Filter(std::string filterText) {

	auto start = std::chrono::steady_clock::now();

	size_t qtd = strategy->Filter(filterText);
	
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;

	getIMessage()->AddMessage("Filter -> %0.2f", elapsed_seconds);
	return qtd;
}

// CLogViewerDoc serialization
BOOL CLogViewerDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	CLogViewerDoc::InitDocument(lpszPathName);

	POSITION pos = GetFirstViewPosition();
	CView* pView = GetNextView(pos);

	// Update Views (simple update - DAG only)
	UpdateAllViews(pView);

	return TRUE;
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CLogViewerDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CLogViewerDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data.
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CLogViewerDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = nullptr;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != nullptr)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CLogViewerDoc diagnostics

#ifdef _DEBUG
void CLogViewerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLogViewerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CLogViewerDoc commands