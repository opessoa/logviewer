#include "pch.h"
#include "InMemoryLogEntryStrategy.h"
#include <fstream>
#include <algorithm>

InMemoryLogEntryStrategy::InMemoryLogEntryStrategy()
{
    filtered = false;
}

bool InMemoryLogEntryStrategy::loadFile(std::string fileName)
{
	ifstream file;
	file.open(fileName, std::ios_base::in | std::ios_base::binary);

	const std::string type[] = { "WARNING", "ERROR", "INFO", "FATAL", "DEBUG" };

	std::string totalbuffer;
	std::string buffer;
	int line = 0;

	size_t oldCurPos = 1, curPos = static_cast<size_t>(file.tellg()) + 1;
	int curLine=-1;
	while (std::getline(file, buffer, '\n')) {
		line++;
		if ((buffer[0] == 'W' && buffer[1] == 'A') ||
			(buffer[0] == 'E' && buffer[1] == 'R') ||
			(buffer[0] == 'I' && buffer[1] == 'N') ||
			(buffer[0] == 'F' && buffer[1] == 'A') ||
			(buffer[0] == 'D' && buffer[1] == 'E')
			)
		{
			if (totalbuffer.size() && totalbuffer != buffer)
			{
				lineVector.push_back(fromString(curLine, totalbuffer));
			}
			curLine = line;
			totalbuffer = buffer;
		}
		else {
			totalbuffer += buffer;
		}
		curPos = static_cast<size_t>(file.tellg());
	}
	if (totalbuffer.size() && totalbuffer != buffer)
	{
		lineVector.push_back(fromString(curLine, totalbuffer));
	}

	file.close();

    return false;
}

const std::vector<std::string>& InMemoryLogEntryStrategy::operator[](int lineNum)
{
	static int lastLine = 0;
	static std::vector<std::string> line;
	if (lineNum != 0 && lastLine == lineNum)
	{
		return line;
	}
	line.clear();
	lastLine = lineNum;
	const auto & linePositionVector = GetLinePosition();
	if (linePositionVector.size() > lineNum) {
		line = linePositionVector[lineNum];
	}

    return line;
}

size_t InMemoryLogEntryStrategy::Filter(std::string filterText)
{
	if (filterText.empty()) {
		filtered = false;
		return GetRecordCount();
	}
	lineVectorFiltered.clear();
	/*auto it = std::partition(lineVector.begin(), lineVector.end(), [&filterText](auto& logVector) {
		return logVector[8].find(filterText) != std::string::npos;
	});*/
	std::copy_if(lineVector.begin(), lineVector.end(), std::back_inserter(lineVectorFiltered), [&filterText](const auto& logVector) {
		return logVector[8].find(filterText) != std::string::npos;
	});

	filtered = true;

    return lineVectorFiltered.size();
}

size_t InMemoryLogEntryStrategy::GetRecordCount()
{
	return GetLinePosition().size(); 
}


std::vector<std::string> InMemoryLogEntryStrategy::fromString(int lineNum, const std::string& lineText) {
	std::vector<std::string> line;
	size_t befPos, pos;
	if (lineText[0] == '\n')
		befPos = 1;
	pos = lineText.find('\t', 0);
	if (pos) {
		line.push_back(std::to_string(lineNum));
		line.push_back(lineText.substr(0, pos));
		befPos = pos + 1;
		pos = lineText.find('\t', befPos);
		if (pos) {
			line.push_back(lineText.substr(befPos, pos - befPos));
			befPos = pos + 1;
			pos = lineText.find('\t', befPos);
			if (pos) {
				line.push_back(lineText.substr(befPos, pos - befPos));
				befPos = pos + 1;
				pos = lineText.find('\t', befPos);
				if (pos) {
					line.push_back(lineText.substr(befPos, pos - befPos));
					befPos = pos + 1;
					pos = lineText.find('\t', befPos);
					if (pos) {
						line.push_back(lineText.substr(befPos, pos - befPos));
						befPos = pos + 1;
						pos = lineText.find('@', befPos);
						if (pos) {
							line.push_back(lineText.substr(befPos, pos - befPos));
							befPos = pos + 1;
							pos = lineText.find('\t', befPos);
							if (pos) {
								line.push_back(lineText.substr(befPos, pos - befPos));
								befPos = pos + 1;
								line.push_back(lineText.substr(befPos, -1));
							}
						}
					}

				}
			}
		}

	}
	return line;
}