
#include "pch.h"
#include "framework.h"

#include "OutputWnd.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _WM_CREATE_OUTPUT_LIST WM_USER + 1

/////////////////////////////////////////////////////////////////////////////
// COutputBar

COutputWnd::COutputWnd() noexcept
{
}

COutputWnd::~COutputWnd()
{
}

BEGIN_MESSAGE_MAP(COutputWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(_WM_CREATE_OUTPUT_LIST, &COutputWnd::OnCreateOutputList)
END_MESSAGE_MAP()

LRESULT COutputWnd::OnCreateOutputList(WPARAM wParam, LPARAM lParam)
{
	if (!m_wndTabs.m_hWnd)
	{
		return 0;
	}

	std::string sName = *(std::string*)lParam;
	auto it = outputListMap.find(sName);
	if (it == outputListMap.end())
	{
		std::shared_ptr<COutputList> pOutList(new COutputList());

		pOutList->Create(LBS_HASSTRINGS | LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWFIXED | WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL, CRect(0, 0, 0, 0), &m_wndTabs, (UINT)outputListMap.size() + 2);
		pOutList->SetFont(&m_font);

		ASSERT_VALID(pOutList.get());

		outputListMap[sName] = pOutList;

		if (m_wndTabs.m_hWnd)
		{
			m_wndTabs.AddTab(pOutList.get(), CString(sName.c_str()), (UINT)21);

			AdjustHorzScroll(*pOutList);
		}
	}

	return 0;
}

int COutputWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create tabs window:
	if (!m_wndTabs.Create(CMFCTabCtrl::STYLE_FLAT, rectDummy, this, 1))
	{
		TRACE0("Failed to create output tab window\n");
		return -1;      // fail to create
	}

	m_font.CreatePointFont(100, _T("Arial"));

	UpdateFonts();

	return 0;
}

void COutputWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	// Tab control should cover the whole client area:
	m_wndTabs.SetWindowPos (nullptr, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

void COutputWnd::AdjustHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	CFont* pOldFont = dc.SelectObject(&m_font);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		cxExtentMax = max(cxExtentMax, (int)dc.GetTextExtent(strItem).cx);
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(pOldFont);
}

void COutputWnd::UpdateFonts()
{
	for (auto it = outputListMap.begin(); it != outputListMap.end(); it++)
	{
		it->second->SetFont(&m_font);
	}
}

void COutputWnd::AddMessage(char const* const _Format, ...)
{
	static char _Buffer[2048];
	va_list _ArgList;
	__crt_va_start(_ArgList, _Format);

#pragma warning(push)
#pragma warning(disable: 4996) // Deprecation
	_vsprintf_l(_Buffer, _Format, NULL, _ArgList);
#pragma warning(pop)

	__crt_va_end(_ArgList);

	AddMessage(std::string(_Buffer));
}

void COutputWnd::AddMessage(std::string strMsg)
{
	AddTextToWindow("Mensagens", CString(strMsg.c_str()), RGB(0, 0, 165));
}

void COutputWnd::AddTextToWindow(std::string window, CString sText, COLORREF color)
{
	if (!IsWindow(this->m_hWnd))
		return;

	std::shared_ptr<COutputList> lstWnd = CreateOutputList(window);
	if (lstWnd && IsWindow(lstWnd->m_hWnd))
	{
		CString sAux;// (MTRoteiriza::MTHelper::Now().c_str());
		sAux += " - ";
		sAux += sText;
		lstWnd->AddString(sAux, color);
		lstWnd->SetTopIndex(lstWnd->GetCount() - 1);
	}
}

void COutputWnd::AddText(std::string sWindow, std::string strMsg, COLORREF color)
{
	if (IsWindow(this->m_hWnd))
		AddTextToWindow(sWindow, CString(strMsg.c_str()), color);
}

std::shared_ptr<COutputList> COutputWnd::CreateOutputList(std::string sName)
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());

	SendMessage(_WM_CREATE_OUTPUT_LIST, 0, (LPARAM)&sName);
	auto it = outputListMap.find(sName);
	if (it != outputListMap.end())
		return it->second;

	return nullptr;
}

/////////////////////////////////////////////////////////////////////////////
// COutputList1

COutputList::COutputList() noexcept
{
}

COutputList::~COutputList()
{
}

BEGIN_MESSAGE_MAP(COutputList, CListBox)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_VIEW_OUTPUTWND, OnViewOutput)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// COutputList message handlers

void COutputList::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	CMenu menu;
	menu.LoadMenu(IDR_OUTPUT_POPUP);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}

	SetFocus();
}

void COutputList::OnEditCopy()
{
	MessageBox(_T("Copy output"));
}

void COutputList::OnEditClear()
{
	MessageBox(_T("Clear output"));
}

void COutputList::OnViewOutput()
{
	CDockablePane* pParentBar = DYNAMIC_DOWNCAST(CDockablePane, GetOwner());
	CMDIFrameWndEx* pMainFrame = DYNAMIC_DOWNCAST(CMDIFrameWndEx, GetTopLevelFrame());

	if (pMainFrame != nullptr && pParentBar != nullptr)
	{
		pMainFrame->SetFocus();
		pMainFrame->ShowPane(pParentBar, FALSE, FALSE, FALSE);
		pMainFrame->RecalcLayout();

	}
}