
// LogViewerView.cpp : implementation of the CLogViewerView class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "LogViewer.h"
#endif

#include "LogViewerDoc.h"
#include "LogViewerView.h"
#include "IApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CLogViewerView

IMPLEMENT_DYNCREATE(CLogViewerView, CListView)

BEGIN_MESSAGE_MAP(CLogViewerView, CListView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDrawList)
	ON_COMMAND(ID_EDIT_FIND, &CLogViewerView::OnEditFind)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CLogViewerView construction/destruction

CLogViewerView::CLogViewerView() noexcept
{
	// TODO: add construction code here
	m_dwDefaultStyle |= ( LVS_REPORT | LVS_OWNERDATA );
	useCache = false;

}

CLogViewerView::~CLogViewerView()
{
}

BOOL CLogViewerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CListView::PreCreateWindow(cs);
}

void CLogViewerView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	LV_COLUMN lvc;

	lvc.mask = LVCF_TEXT | LVCF_SUBITEM | LVCF_WIDTH;

	lvc.iSubItem = 0;
	lvc.pszText = _T("Line");
	lvc.cx = 55;
	GetListCtrl().InsertColumn(0, &lvc);
	lvc.iSubItem = 1;
	lvc.pszText = _T("Level");
	lvc.cx = 75;
	GetListCtrl().InsertColumn(1, &lvc);
	lvc.iSubItem = 2;
	lvc.pszText = _T("Date");
	lvc.cx = 125;
	GetListCtrl().InsertColumn(2, &lvc);
	lvc.iSubItem = 3;
	lvc.pszText = _T("Thread");
	lvc.cx = 75;
	GetListCtrl().InsertColumn(3, &lvc);
	lvc.iSubItem = 4;
	lvc.pszText = _T("Library");
	lvc.cx = 150;
	GetListCtrl().InsertColumn(4, &lvc);
	lvc.iSubItem = 5;
	lvc.pszText = _T("Logger");
	lvc.cx = 150;
	GetListCtrl().InsertColumn(5, &lvc);
	lvc.iSubItem = 6;
	lvc.pszText = _T("Source");
	lvc.cx = 150;
	GetListCtrl().InsertColumn(6, &lvc);
	lvc.iSubItem = 7;
	lvc.pszText = _T("Line");
	lvc.cx = 150;
	GetListCtrl().InsertColumn(7, &lvc);
	lvc.iSubItem = 8;
	lvc.pszText = _T("Message");
	lvc.cx = 250;
	GetListCtrl().InsertColumn(8, &lvc);
	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().
	GetListCtrl().SetExtendedStyle(LVS_EX_FULLROWSELECT);
}

void CLogViewerView::PrepareCache(int iFrom, int iTo)
{
	if (!useCache)
		return;

	cache.clear();
	CLogViewerDoc& pLogDoc = *GetDocument();
	for (; iFrom <= iTo; iFrom++) {
		cache.insert(std::make_pair(iFrom, pLogDoc[iFrom]));
	}
}

void CLogViewerView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
	UpdateFilter(_T("Stored"));
}

void CLogViewerView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
	UpdateFilter(_T("Requesting"));
	auto parent = dynamic_cast<IApp*>(GetParentFrame());
	auto imess = parent->GetIMessage();
	imess->AddMessage("Ola Mundo");
}


// CLogViewerView diagnostics

#ifdef _DEBUG
void CLogViewerView::AssertValid() const
{
	CListView::AssertValid();
}

void CLogViewerView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CLogViewerDoc* CLogViewerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLogViewerDoc)));
	return (CLogViewerDoc*)m_pDocument;
}
#endif //_DEBUG


// CLogViewerView message handlers
void CLogViewerView::GetDispInfo(LVITEM* pItem)
{
	CLogViewerDoc& pLogDoc = *GetDocument();

	// called when the listview needs to display data
	if (pItem->mask & LVIF_TEXT)
	{
		// first, move to the appropriate row in the database
		std::vector<std::string> line;
		if (useCache)
			line = cache[pItem->iItem];
		else
			line = pLogDoc[pItem->iItem];
		if (!line.empty())
		{
			if (line.size() > pItem->iSubItem) {
				std::wstring ws(line[pItem->iSubItem].begin(), line[pItem->iSubItem].end());
				_tcscpy_s(pItem->pszText, pItem->cchTextMax, ws.substr(0,259).c_str());
			}
		}
	}
}

BOOL CLogViewerView::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	if (message == WM_NOTIFY)
	{
		NMHDR* phdr = (NMHDR*)lParam;

		// these 3 notifications are only sent by virtual listviews
		switch (phdr->code)
		{
		case LVN_GETDISPINFO:
		{
			NMLVDISPINFO* pLvdi;

			pLvdi = (NMLVDISPINFO*)lParam;
			GetDispInfo(&pLvdi->item);
		}
		if (pResult != NULL)
		{
			*pResult = 0;
		}
		break;
		case LVN_ODCACHEHINT:
		{
			NMLVCACHEHINT* pHint = (NMLVCACHEHINT*)lParam;

			PrepareCache(pHint->iFrom, pHint->iTo);
		}
		if (pResult != NULL)
		{
			*pResult = 0;
		}
		break;
		case LVN_ODFINDITEM:
		{
			NMLVFINDITEM* pFindItem = (NMLVFINDITEM*)lParam;
			int i = FindItem(pFindItem->iStart, pFindItem);
			if (pResult != NULL)
			{
				*pResult = i;
			}
		}
		break;
		default:
			return CListView::OnChildNotify(message, wParam, lParam, pResult);
		}
	}
	else
		return CListView::OnChildNotify(message, wParam, lParam, pResult);

	return TRUE;
}

int CLogViewerView::FindItem(int iStart, LPNMLVFINDITEM plvfi)
{
	if ((plvfi->lvfi.flags & LVFI_STRING) == 0)
	{
		//This will probably never happend...
		return -1;
	}
	CListCtrl& list = GetListCtrl();
	//This is the string we search for
	CString searchstr = plvfi->lvfi.psz;

	int startPos = plvfi->iStart;
	//Is startPos outside the list (happens if last item is selected)
	if (startPos >= list.GetItemCount())
		startPos = 0;

	int currentPos = startPos;

	CLogViewerDoc& pLogDoc = *GetDocument();
	//Let's search...
	do
	{
		std::vector<std::string> line;
		if (useCache)
			line = cache[currentPos];

		if (line.empty())
			line = pLogDoc[currentPos];

		if (!line.empty())
		{
			//Do this word begins with all characters in searchstr?
			if (_tcsstr(CString(line[8].c_str()), searchstr) != nullptr)
			{
				return currentPos;
			}
		}

		//Go to next item
		currentPos++;

		//Need to restart at top?
		if (currentPos >= list.GetItemCount())
			currentPos = 0;

		//Stop if back to start
	} while (currentPos != startPos);
	return -1;
}

void CLogViewerView::UpdateFilter(CString strCurQuery)
{
	// convenience function to set the SQL filter for the query

	CLogViewerDoc& pLogDoc = *GetDocument();

	int nRecordCount = 0;
	//if (pLogDoc.IsOpen())
	{
		pLogDoc.Filter(std::string(CStringA(strCurQuery).GetBuffer()));
		nRecordCount = static_cast<int>(pLogDoc.GetRecordCount());
	}
	GetListCtrl().SetItemCountEx(nRecordCount);
	GetListCtrl().Invalidate();
	GetListCtrl().UpdateWindow();
}

void CLogViewerView::OnCustomDrawList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLVCUSTOMDRAW lpLVCustomDraw = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
	if (lpLVCustomDraw->iSubItem == 0)
	{
		CString sText = GetListCtrl().GetItemText(static_cast<int>(((NMCUSTOMDRAW*)lpLVCustomDraw)->dwItemSpec), 1);// lpLVCustomDraw->iSubItem);
		switch (lpLVCustomDraw->nmcd.dwDrawStage)
		{
		case CDDS_ITEMPREPAINT:
		case CDDS_ITEMPREPAINT | CDDS_SUBITEM:
			lpLVCustomDraw->clrTextBk = RGB(0x37, 0x47, 0x57);
			if (sText == "ERROR")
			{
				//lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);// RGB(0xe4, 0xf0, 0xf9);// RGB(150, 20, 20); // black background
				lpLVCustomDraw->clrText = lpLVCustomDraw->iSubItem == 0 ? RGB(0, 0xf0, 0xf9) : RGB(255, 255, 0); // white text
			}else if(sText == "WARNING")
			{
				//lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);//RGB(0xe4,0xf0,0xf9); //RGB(155, 135, 12); // black background
				lpLVCustomDraw->clrText = lpLVCustomDraw->iSubItem == 0 ? RGB(0, 0xf0, 0xf9) : RGB(0, 0, 0); // white text
			}else if (sText == "INFO")
			{
				//lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);//RGB(173, 216, 230); // black background
				lpLVCustomDraw->clrText = lpLVCustomDraw->iSubItem == 0 ? RGB(0, 255, 255) : RGB(0, 0, 0); // white text
			}
			else if (sText == "FATAL")
			{
				//lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);//RGB(150, 20, 20); // black background
				lpLVCustomDraw->clrText = RGB(155, 135, 12);
			}
			else if (sText == "DEBUG")
			{
				//lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);//RGB(173, 216, 230); // black background
				lpLVCustomDraw->clrText = RGB(0, 0, 0);
			}
			break;
		}
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CLogViewerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	CListView::OnUpdate(pSender, lHint, pHint); 
	UpdateFilter(_T(""));
}


void CLogViewerView::OnEditFind()
{
	UpdateFilter(_T("Stored"));
	/*m_pFindDialog = new CFindReplaceDialog();
	m_pFindDialog->Create(TRUE, L"Initial Text", NULL, FR_DOWN, this);
	CString strName("WF_");
	LV_FINDINFO lvfi;
	lvfi.flags = LVFI_STRING;
	lvfi.psz = strName;
	CListCtrl& list = GetListCtrl();
	static int nItem = -1;
	nItem = list.FindItem(&lvfi, nItem);
	if (nItem != -1) {
		list.EnsureVisible(nItem, FALSE);
		list.SetFocus();
		list.SetItemState(nItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}*/
}


BOOL CLogViewerView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	CBrush backBrush(RGB(0x37, 0x47, 0x57));
	CBrush* pOldBrush = pDC->SelectObject(&backBrush);
	CRect rect;
	pDC->GetClipBox(&rect);
	pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
	pDC->SelectObject(pOldBrush);
	return TRUE;
}
