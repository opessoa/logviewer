#include "pch.h"
#include "CMFCListView.h"

IMPLEMENT_DYNCREATE(CMFCListView, CCtrlView)

CMFCListView::CMFCListView() : CCtrlView(WC_LISTVIEW,
	AFX_WS_DEFAULT_VIEW)
{ }
// NOTE: The cast in GetListCtrl is ugly, but must be preserved for compatibility.
// CListCtrl is not related to CListView by inheritance so we must be careful to ensure 
// that CListCtrl remains a binary compatible subset of CListView.
CMFCListCtrl& CMFCListView::GetListCtrl() const
{
	return *(CMFCListCtrl*)this;
}


BEGIN_MESSAGE_MAP(CMFCListView, CCtrlView)
	ON_WM_NCDESTROY()
END_MESSAGE_MAP()

BOOL CMFCListView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CCtrlView::PreCreateWindow(cs);
}

void CMFCListView::DrawItem(LPDRAWITEMSTRUCT)
{
	//ASSERT(FALSE);
}

BOOL CMFCListView::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam,
	LRESULT* pResult)
{
	if (message != WM_DRAWITEM)
		return CCtrlView::OnChildNotify(message, wParam, lParam, pResult);

	ASSERT(pResult == NULL);       // no return value expected
	UNUSED(pResult); // unused in release builds

	DrawItem((LPDRAWITEMSTRUCT)lParam);
	return TRUE;
}

void CMFCListView::RemoveImageList(int nImageList)
{
	HIMAGELIST h = (HIMAGELIST)SendMessage(LVM_GETIMAGELIST,
		(WPARAM)nImageList);
	if (CImageList::FromHandlePermanent(h) != NULL)
		SendMessage(LVM_SETIMAGELIST, (WPARAM)nImageList, NULL);
}

void CMFCListView::OnNcDestroy()
{
	RemoveImageList(LVSIL_NORMAL);
	RemoveImageList(LVSIL_SMALL);
	RemoveImageList(LVSIL_STATE);

	CCtrlView::OnNcDestroy();
}
