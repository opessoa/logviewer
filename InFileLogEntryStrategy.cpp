#include "pch.h"
#include "InFileLogEntryStrategy.h"
#include <algorithm>

InFileLogEntryStrategy::InFileLogEntryStrategy() {
	filtered = false;
	fileBuf = nullptr;
}

bool InFileLogEntryStrategy::loadFile(std::string fileName)
{
	file.open(fileName, std::ios_base::in | std::ios_base::binary);

	const std::string type[] = { "WARNING", "ERROR", "INFO", "FATAL", "DEBUG" };

	std::string totalbuffer;
	std::string buffer;
	int line = 0;

	size_t oldCurPos = 1, curPos = static_cast<size_t>(file.tellg()) + 1;
	int curLine=-1;
	while (std::getline(file, buffer, '\n')) {
		line++;
		bool achou = false;
		if ((buffer[0] == 'W' && buffer[1] == 'A') ||
			(buffer[0] == 'E' && buffer[1] == 'R') ||
			(buffer[0] == 'I' && buffer[1] == 'N') ||
			(buffer[0] == 'F' && buffer[1] == 'A') ||
			(buffer[0] == 'D' && buffer[1] == 'E')
			)
		{
			if (totalbuffer.size() && totalbuffer != buffer)
			{
				LogLine ll{ curLine, oldCurPos, 0 };
				updateMsgPos(totalbuffer, ll.msgIni, ll.msgFim);
				linePositionVector.push_back(ll);
				oldCurPos = static_cast<size_t>(file.tellg()) - buffer.length();
			}
			curLine = line;
			totalbuffer = buffer;
		}
		else {
			totalbuffer += buffer;
		}
		curPos = static_cast<size_t>(file.tellg());
	}
	if (totalbuffer.size() && totalbuffer != buffer)
	{
		LogLine ll{ curLine, curPos - totalbuffer.length(), 0 };
		updateMsgPos(totalbuffer, ll.msgIni, ll.msgFim);
		linePositionVector.push_back(ll);
	}
	file.seekg(0);
	fileBuf = file.rdbuf();

    return false;
}

const std::vector<std::string>& InFileLogEntryStrategy::operator[](int lineNum)
{
	static int lastLine = 0;
	static std::vector<std::string> line;
	if (lineNum != 0 && lastLine == lineNum)
	{
		return line;
	}
	line.clear();
	lastLine = lineNum;
	const auto& linePositionVector= GetLinePosition();
	if (linePositionVector.size() > lineNum)
	{
		const size_t linePos = linePositionVector[lineNum].ini;
		std::streamsize size = linePositionVector[lineNum].msgIni + linePositionVector[lineNum].msgFim;

		std::string text;
		text.resize(size);

		fileBuf->pubseekoff(linePos - 1, file.beg);       // rewind

		fileBuf->sgetn(&text[0], size - 1);

		line = fromString(linePositionVector[lineNum].linha, text);
	}
	return line;
}


size_t InFileLogEntryStrategy::Filter(std::string filterText) {
	if (filterText.empty()) {
		filtered = false;
		return GetRecordCount();
	}

	std::string text;
	auto fBuf = fileBuf;
	std::copy_if(linePositionVector.begin(), linePositionVector.end(), std::back_inserter(linePositionVectorFiltered), [&fBuf, &filterText,&text](const auto& logLine) {
		fBuf->pubseekoff((logLine.ini + logLine.msgIni - 1), ifstream::beg);
		text.resize(logLine.msgFim);
		fBuf->sgetn(&text[0], logLine.msgFim);

		return text.find(filterText) != std::string::npos;
	});

	filtered = true;

	return GetRecordCount();
}

void InFileLogEntryStrategy::updateMsgPos(const std::string& lineText, size_t& msgPos, size_t& msgSize)
{
	size_t pos = lineText.find('\t', 0);
	if (pos) {
		pos = lineText.find('\t', pos + 1);
		if (pos) {
			pos = lineText.find('\t', pos + 1);
			if (pos) {
				pos = lineText.find('\t', pos + 1);
				if (pos) {
					pos = lineText.find('\t', pos + 1);
					if (pos) {
						pos = lineText.find('@', pos + 1);
						if (pos) {
							pos = lineText.find('\t', pos + 1);
							if (pos) {
								msgPos = pos + 1;
								msgSize = lineText.substr(pos + 1, -1).length();
							}
						}
					}

				}
			}
		}
	}
}

std::vector<std::string> InFileLogEntryStrategy::fromString(int lineNum, const std::string& lineText) {
	std::vector<std::string> line;
	size_t befPos, pos;
	if (lineText[0] == '\n')
		befPos = 1;
	pos = lineText.find('\t', 0);
	if (pos) {
		line.push_back(std::to_string(lineNum));
		line.push_back(lineText.substr(0, pos));
		befPos = pos + 1;
		pos = lineText.find('\t', befPos);
		if (pos) {
			line.push_back(lineText.substr(befPos, pos - befPos));
			befPos = pos + 1;
			pos = lineText.find('\t', befPos);
			if (pos) {
				line.push_back(lineText.substr(befPos, pos - befPos));
				befPos = pos + 1;
				pos = lineText.find('\t', befPos);
				if (pos) {
					line.push_back(lineText.substr(befPos, pos - befPos));
					befPos = pos + 1;
					pos = lineText.find('\t', befPos);
					if (pos) {
						line.push_back(lineText.substr(befPos, pos - befPos));
						befPos = pos + 1;
						pos = lineText.find('@', befPos);
						if (pos) {
							line.push_back(lineText.substr(befPos, pos - befPos));
							befPos = pos + 1;
							pos = lineText.find('\t', befPos);
							if (pos) {
								line.push_back(lineText.substr(befPos, pos - befPos));
								befPos = pos + 1;
								line.push_back(lineText.substr(befPos, -1));
							}
						}
					}

				}
			}
		}

	}
	return line;
}
