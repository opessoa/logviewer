#pragma once

#include<fstream>
#include "ILogEntryStrategy.h"

class InFileLogEntryStrategy : public ILogEntryStrategy
{
public:
	InFileLogEntryStrategy();

protected:
	typedef struct {
		int linha;
		size_t ini;
		size_t msgIni;
		size_t msgFim;
	}LogLine;

public:
	bool loadFile(std::string fileName) override;
	const std::vector<std::string>& operator[](int lineNum) override;

	size_t GetRecordCount() {
		return GetLinePosition().size();
	}

	std::vector<LogLine>& GetLinePosition() {
		if (filtered) {
			return linePositionVectorFiltered;
		}
		return linePositionVector;
	}

	size_t Filter(std::string filterText) override;

protected:
	std::vector<std::string> fromString(int lineNum, const std::string& line);
	void updateMsgPos(const std::string& lineText, size_t& msgPos, size_t& msgSize);

	std::vector<LogLine> linePositionVector;
	std::vector<LogLine> linePositionVectorFiltered;
	std::ifstream file;
	std::filebuf* fileBuf;
	std::string fileName;
};

