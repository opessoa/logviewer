
// LogViewerDoc.h : interface of the CLogViewerDoc class
//

#include<string>
#include<vector>
#include<fstream>
#include<map>
#include "IApp.h"
#include "ILogEntryStrategy.h"

#pragma once


class CLogViewerDoc : public CDocument
{
protected: // create from serialization only
	CLogViewerDoc() noexcept;
	DECLARE_DYNCREATE(CLogViewerDoc)

// Attributes
public:
	std::shared_ptr<ILogEntryStrategy> strategy;
	
// Operations
public:
	std::vector<std::string> operator[](int);
	void InitDocument(LPCTSTR lpszPathName);
	size_t Filter(std::string filter);
// Overrides
public:
	virtual BOOL OnNewDocument();
	
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName); 
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CLogViewerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS

public:
	bool Filtered() { return strategy->Filtered(); }
	
	size_t GetRecordCount() {
		return strategy->GetRecordCount();
	}

protected:
	IMessage* getIMessage() {
		POSITION pos = GetFirstViewPosition();
		CView* pView = GetNextView(pos);
		auto parent = dynamic_cast<IApp*>(pView->GetParentFrame());
		return parent->GetIMessage();
	}
};

